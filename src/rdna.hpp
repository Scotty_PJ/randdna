#include <string>
#include <random>
#include <iostream>
#include <vector>

using std::string;

string randDNA(int seed, string bases, int n)
{
	std::mt19937 gen(seed);
	std::uniform_int_distribution<> uniform(1,bases.size());
	string ranstring;
	
	
	for (int i =0; i < n; i++)
	{
		ranstring += bases[uniform(gen) - 1];
	}
	
	return ranstring;

}

/*
string randDNA(int seed, string bases, int n)
{
	std::mt19937 gen(seed);
	std::uniform_int_distribution<> uniform(1,bases.size());
	std::vector <char> ranstring;
	string newstring;
	
	
	for (int i =0; i < n; i++)
	{
		ranstring.push_back( bases[uniform(gen) - 1]);
	}
	newstring (ranstring.begin(), ranstring.end());
	return newstring;

}*/
